var afterRecoveryFilter = require('../after-recovery-filter-sync');
var fs = require('fs');
var assert = require('assert');

describe('afterRecoveryFilter', function() {
    describe('removeUselessFiles', function() {
        var testFiles = [
            'test/useless/notimage.txt',
            'test/useless/small.jpg',
            'test/useless/small.mpg'
        ];

        before(function() {
            testFiles.forEach(function(file) {
                fs.writeFile(file, file);
            });
        });

        it('should remove all files smaller than 100 MB and return these in removed files', function() {
            var removedFiles = afterRecoveryFilter.removeUselessFiles(['test/useless']);

            console.log('Removed files:');
            console.log(removedFiles);

            assert.equal('test/useless/notimage.txt', removedFiles[0]);
            assert.equal('test/useless/small.jpg', removedFiles[1]);
            assert.equal('test/useless/small.mpg', removedFiles[2]);
            assert(!fs.existsSync('test/useless/notimage.txt'));
            assert(!fs.existsSync('test/useless/small.jpg'));
            assert(!fs.existsSync('test/useless/small.mpg'));
        });

        it('should keep .jpg file higher than 100 MB', function() {
            var removedFiles = afterRecoveryFilter.removeUselessFiles(['test/useless']);

            assert(fs.existsSync('test/useless/photo.jpg'));
        });

        it('should keep .mpg file higher than 100 MB', function() {
            var removedFiles = afterRecoveryFilter.removeUselessFiles(['test/useless']);

            assert(fs.existsSync('test/useless/video.mpg'));
        });

    });
});
