var duplicatesFinder = require('../duplicates-finder-sync');
var fs = require('fs');
var assert = require('assert');

describe('duplicatesFinderSync', function() {
    describe('findDuplicates', function() {
        var hashcode = '6d5333849ebe0a631a0bae336c456dcc492fb7a2';
        var dirs = ['test/firstDir', 'test/secondDir'];

        it('should return 2 duplicities', function() {
            var duplicities = duplicatesFinder.findDuplicates(dirs);

            console.log(duplicities);

            assert.equal(2, duplicities[hashcode].length);
        });

        it('should return expected filename for given hashcode', function() {
            var duplicities = duplicatesFinder.findDuplicates(dirs);

            console.log(duplicities);

            assert.equal(
                'test/firstDir/image-v1.jpg',
                duplicities[hashcode][0]
            );
        });

        it('should return different filenames for given hashcode', function() {
            var duplicities = duplicatesFinder.findDuplicates(dirs);

            console.log(duplicities);

            assert.notEqual(duplicities[hashcode][0],
                            duplicities[hashcode][1]);
        });
    });

    describe('removeDuplicates', function() {
        var testFiles = [
            'test/file1-v2',
            'test/file2-v2'
        ];

        before(function() {
            testFiles.forEach(function(file) {
                fs.writeFile(file, file);
            });
        });

        after(function() {
//             testFiles.forEach(function(file) {
//                 if (fs.existsSync(file)) {
//                     fs.unlinkSync(file);
//                 }
//             });
        });

        it('should remove duplicities and return removed files', function() {

            var duplicities = {
                'hash1': ['test/file1-v1', 'test/file1-v2'],
                'hash2': ['test/file2-v1', 'test/file2-v2']
            };

            console.log(duplicities);
            var removedFiles = duplicatesFinder.removeDuplicates(duplicities);

            assert.equal('test/file1-v2', removedFiles[0]);
            assert.equal('test/file2-v2', removedFiles[1]);

            assert(!fs.existsSync('test/file1-v2'));
            assert(!fs.existsSync('test/file2-v2'));
        });
    });
});
