var fs = require('fs');
var path = require('path');
var crypto = require('crypto');


/**
 * public method
 */
var findDuplicates = function(dirs) {
    var hashcodes = {};

    // count hashcodes for input dirs
    dirs.forEach(function(dir) {
        addFiles(dir, hashcodes);
    });

    // compare and return filenames of duplicates
    return findHashcodesWithMoreFiles(hashcodes);
};

/**
 * public method
 */
var removeDuplicates = function(duplicities) {
    var removedFiles = [];
    for (hashcode in duplicities) {
        var files = duplicities[hashcode];

        files.forEach(function(file, index) {
            if (index > 0) {
                fs.unlinkSync(file)
                removedFiles.push(file);
                console.log(file);
            }
        });
    }

    return removedFiles;
};

var addFiles = function(dir, hashcodes) {
    var files = fs.readdirSync(dir);
    files.forEach(function(file) {
	    var filename = path.join(dir, file);  
		
        if (!fs.statSync(filename).isDirectory()) {
	        var digest = countHash(filename);

            if (hashcodes[digest] === undefined) {
                hashcodes[digest] = [];
            }

	        hashcodes[digest].push(filename);
		}
    });
};

var countHash = function(file) {
    var data = fs.readFileSync(file);
    var hash = crypto.createHash('sha1');
    return hash.update(data).digest('hex');
}

var findHashcodesWithMoreFiles = function(hashcodes) {
    var result = {};
    for (hashCode in hashcodes) {
        if (hashcodes[hashCode].length > 1) {
            result[hashCode] = hashcodes[hashCode];
            console.log(result[hashCode]);
        }
    };
    return result;
};

module.exports = {
    findDuplicates: findDuplicates,
    removeDuplicates: removeDuplicates
};
