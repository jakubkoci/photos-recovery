# Photo recovery utility

Command-line application for process some files (photos). I wrote this to remove duplicated photos and useless files after recovery with [PhotoRec](http://www.cgsecurity.org/wiki/PhotoRec).

Now it's workning only sync version. Async is totally incomplete.

### Install
```
npm install
```

### Run test
```
npm test
```

### Run
```
node main.js dirs.json
```
Argument dirs.json should be JSON file that contains list of dirs to process.

* Script removes small and not image/video files
* Finds duplicities between remaining files accoring their hashcodes
* Removes all duplicated files