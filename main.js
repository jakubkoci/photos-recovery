var duplicateFinderSync = require('./duplicates-finder-sync');
var afterRecoveryFilter = require('./after-recovery-filter-sync');
var fs = require('fs');

var args = process.argv.slice(2);

if (args.length <= 0) {
    console.log('No config file!');
    process.exit(1);
}

var configFile = args[0];
if (!fs.existsSync(configFile)) {
    console.log('Given config file does not exists!');
    process.exit(1);
}

var configData = fs.readFileSync(configFile);
var configJSON = JSON.parse(configData);
var dirs = configJSON.dirs;

console.log('\nInput dirs:');
console.log(dirs);

console.log('\nRemoving useless files...');
afterRecoveryFilter.removeUselessFiles(dirs);

console.log('\nFinding duplicities...');
var duplicities = duplicateFinderSync.findDuplicates(dirs);

console.log('\nRemoving duplicities...');
var removedFiles = duplicateFinderSync.removeDuplicates(duplicities);
