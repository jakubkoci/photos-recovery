var fs = require('fs');
var path = require('path');
var crypto = require('crypto');
var events = require('events');

var eventEmitter = new events.EventEmitter();
var results = {};

// find duplicates
var findDuplicates = function(firstDir, secondDir) {
    // count source hash codes
    firstHashCodes = countFilesHashCodes(firstDir);

    // count target hash codes
    secondHashCodes = countFilesHashCodes(secondDir);

    console.log('after all');
    // compare and return filenames of duplicates
};

var countFilesHashCodes = function(dir) {
    fs.readdir(dir, function(err, files) {
        if (err) throw err;

        files.forEach(function(file) {
            countHash(path.join(dir, file));
        });
    });
};

var countHash = function(file) {
    var readStream = fs.createReadStream(file);
    var hash = crypto.createHash('sha1');

    readStream.on('data', function(chunk) {
        hash.update(chunk);
    });

    readStream.on('end', function(chunk) {
        var digest = hash.digest('hex');
        eventEmitter.emit('digest', digest, file);
    });
}

eventEmitter.on('digest', function(digest, file) {
    if (results[digest] === undefined) {
        results[digest] = file;
    } else {
        console.log(digest + ": " + file);
    }
});

module.exports = {
    findDuplicates: findDuplicates
};
