var fs = require('fs');
var path = require('path');

/**
 * public method
 */
var removeUselessFiles = function(dirs) {
    var removedFiles = [];
    dirs.forEach(function(dir) {
        removedFiles = removeUselessFilesInDir(dir);
    });
    return removedFiles;
};

var removeUselessFilesInDir = function(dir) {
    var removedFiles = [];

    var files = fs.readdirSync(dir);
    files.forEach(function(file, index) {
        var filepath = path.join(dir, file);
        if (!fs.statSync(filepath).isDirectory()) {        
            if (!isUsefulFile(filepath)) {
                console.log('Removing file: ' + filepath);
                fs.unlinkSync(filepath);
                removedFiles.push(filepath);
            }
        }
    });

    return removedFiles;
};

/**
 * If file is image or video with size higher than 100 MB then it's consider as useful
 */
var isUsefulFile = function(file) {
    var stats = fs.statSync(file);
    var size = stats.size / 1024;
    var extension = path.extname(file).toLowerCase();

    console.log("Filter file: ", file, size, extension);
    if ((extension === '.jpg' || extension === '.mpg') && size > 100) {
        return true;
    }

    return false;
};

module.exports = {
    removeUselessFiles: removeUselessFiles
};
